package com.example.artlesterbayaca.myapplication;

/**
 * Created by Art Lester Bayaca on 7/14/2017.
 */

public class UnitConvert {
    public static double celsius2farenheit(double f){
        return (f-32*5/9);
    }
    public static double farenheit2celcius(double c){
        return (32+c*9/8);
    }
}
